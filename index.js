const express = require("express"),
  bodyParser = require("body-parser"),
  config = require("./utils/config")
//创建应用程序
const app = express()

//全局注册使用bodyParser,处理json，raw,URL-encode格式请求体等
app.use(bodyParser.json())
//create application/x-www-form-urlencoded parser
app.use(bodyParser.urlencoded({ extended: false }))

// 写接口
app.get('/', function(request, response) { 
    // 返回数据
    let result =  `这是首页 <a href='/tengfei'>我的主页</a>`
    response.send(result);
})
app.get('/tengfei', function(request, response,next) { 
    // 返回数据
    let result = {
         data : {
            pageIndex: 1,
            pageSize: 10,
            schoolList: [
                {schoolName: '清华大学', palce: '北京'},
                {schoolName: '郑州大学', palce: '郑州'},
                {schoolName: '南开大学', palce: '天津'}],
            code: 10000,
            msg: '查询列表成功' 
            }
    }
    let tengfei=`<div style='color:blue;height:300px;display:flex;justify-content:center;align-items: center;'> 恭喜<span style="font-size:16px;color:red"> 滕飞 </span>同学从0到1成功创建第一个node项目</div> `
    response.send(tengfei);
    next()
})

//路由设置

const baseUrl11 = "/api";
// const user = require('./router/home/user')
//路由挂载
//个人中心路由
// app.use(`${baseUrl}/user`,user)
// const home = require('./router/home/home')
//首页
// app.use(`${baseUrl}`,home)

// 404
app.use(function(req, res, next) {
  return res.status(404).send(responseJson.errorJson(apiError.NOT_FIND))
});

// 500 - Any server error
app.use(function(err, req, res, next) {
  return res.status(500).send(responseJson.errorJson(apiError.NET_FAIL))
});

let ip = config.baseConfig.ip
let address = config.baseConfig.address
app.listen(ip,()=>{
  console.log(address+":"+ip);
})